package user

import (
	"majoo/domain/user"
	"majoo/domain/user/model"
	"majoo/domain/user/repository"
	"majoo/lib/response"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type controller struct {
	service *user.Service
}

func NewController(db *gorm.DB) *controller {
	return &controller{service: user.NewUserService(repository.NewUserRepository(db))}
}

func (c *controller) SignIn(ctx *gin.Context) {
	var req model.UserSignInReq
	if req.Err = ctx.ShouldBindJSON(&req.Body); req.Err != nil {
		response.Error(ctx, http.StatusBadRequest, req.Err.Error())
		return
	}
	res, err := c.service.SignIn(&req)
	if err != nil {
		response.Error(ctx, http.StatusInternalServerError, err.Error())
		return
	}
	response.Success(ctx, http.StatusOK, res.Body)
}

func (c *controller) SignOut(ctx *gin.Context) {
	user, err := c.service.Auth(ctx.Request.Header.Get("Authorization"))
	if err != nil {
		response.Error(ctx, http.StatusUnauthorized, err.Error())
		return
	}
	if err = c.service.SignOut(&user); err != nil {
		response.Error(ctx, http.StatusInternalServerError, err.Error())
		return
	}
	response.Success(ctx, http.StatusOK, nil)
}
