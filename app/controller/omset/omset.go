package omset

import (
	"majoo/domain/omset"
	"majoo/domain/omset/model"
	"majoo/domain/omset/repository"
	user "majoo/domain/user"
	userRepository "majoo/domain/user/repository"
	"strconv"

	"majoo/lib/response"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type controller struct {
	service     *omset.Service
	userService *user.Service
}

func NewController(db *gorm.DB) *controller {
	return &controller{service: omset.NewOmsetService(repository.NewOmsetRepository(db)), userService: user.NewUserService(userRepository.NewUserRepository(db))}
}

func (c *controller) List(ctx *gin.Context) {
	page := ctx.DefaultQuery("page", "1")
	perPage := ctx.DefaultQuery("limit", "1")
	outlet := ctx.DefaultQuery("outlet", "true")
	outletBool, err:= strconv.ParseBool(outlet)
	if err != nil {
		response.Error(ctx, http.StatusBadRequest, err.Error())
		return
	}

	convPage, _ := strconv.Atoi(page)
	limit, _ := strconv.Atoi(perPage)
	offset := (convPage - 1) * limit

	var req model.ListReq
	user, err := c.userService.Auth(ctx.Request.Header.Get("Authorization"))
	if err != nil {
		response.Error(ctx, http.StatusUnauthorized, err.Error())
		return
	}
	if err := ctx.BindQuery(&req); err != nil {
		response.Error(ctx, http.StatusBadRequest, err.Error())
		return
	}

	req.End = req.End + " 23:59:59"
	req.Start = req.Start + " 00:00:00"
	if outletBool {
		res, totalRow, err := c.service.GetList(offset, limit, int(user.ID), req)
		if err != nil {
			response.Error(ctx, http.StatusInternalServerError, err.Error())
		}
		data := response.PaginationResponse(http.StatusOK, totalRow, page, perPage, res)
		response.Success(ctx, http.StatusOK, data)
	}else {
		res, totalRow, err := c.service.GetMerchantList(offset, limit, int(user.ID), req)
		if err != nil {
			response.Error(ctx, http.StatusInternalServerError, err.Error())
		}
		data := response.PaginationResponse(http.StatusOK, totalRow, page, perPage, res)
		response.Success(ctx, http.StatusOK, data)
	}
}
