package healthcheck

import (
	"majoo/db"
	"majoo/lib/response"
	"net/http"

	"github.com/gin-gonic/gin"
)

type controller struct{}

func NewController() *controller {
	return &controller{}
}

func (c *controller) Index(ctx *gin.Context) {
	if err := db.SqlDb.Ping(); err != nil {
		response.Error(ctx, http.StatusInternalServerError, err.Error())
		return 
	}
	response.Success(ctx, http.StatusOK, nil)
}