package main

import (
	"majoo/config"
	"majoo/db"
	"log"

	_ "github.com/joho/godotenv/autoload"
)

func main() {
	gormDb := db.Open()
	defer func() {
		if err := db.SqlDb.Close(); err != nil {
			log.Fatal(err)
		}
	}()
	config.Router(gormDb)
}