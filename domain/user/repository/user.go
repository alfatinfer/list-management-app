package repository

import (
	"gorm.io/gorm"
	"majoo/domain/user/model"
)

type (
	RepositoryInterface interface {
		FirstByEmail(email *string) (model.User, error)
		UpdateToken(*model.User, *string) error
		FirstByID(*uint64) (model.User, error)
		DeleteToken(*model.User) error
	}

	repository struct {
		DB *gorm.DB
	}
)

func NewUserRepository(DB *gorm.DB) RepositoryInterface {
	return &repository{
		DB: DB,
	}
}

func (r *repository) FirstByEmail(UserName *string) (user model.User, err error) {
	return user, r.DB.Where("user_name = ?", UserName).First(&user).Error
}

func (r *repository) UpdateToken(user *model.User, ss *string) error {
	user.Token = *ss
	return r.DB.Save(&user).Error
}

func (r *repository) FirstByID(id *uint64) (user model.User, err error) {
	return user, r.DB.First(&user, id).Error
}

func (r *repository) DeleteToken(admin *model.User) error {
	admin.Token = ""
	return r.DB.Save(&admin).Error
}