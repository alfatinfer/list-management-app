package model

type (
	UserSignInReq struct {
		Body struct {
			UserName string `binding:"required" json:"user_name"`
			Password string `binding:"required" json:"password"`
		}
		Err error
	}

	UserSignInRes struct {
		Body struct {
			Token string `json:"token"`
		}
	}
)
