package model

import (
	"github.com/dgrijalva/jwt-go"
)

type (
	Claims struct {
		ID       uint64 `json:"id"`
		UserName string `json:"user_name"`
		*jwt.StandardClaims
	}
)
