package model

import "time"

type (
	User struct {
		ID        uint64
		UserName  string
		Password  string
		Token     string
		CreatedAt time.Time
		UpdatedAt time.Time
	}
)
