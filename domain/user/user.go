package user

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"majoo/domain/user/model"
	"majoo/domain/user/repository"
	"majoo/lib/encrypt"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type Service struct {
	Repository repository.RepositoryInterface
}

func NewUserService(repository repository.RepositoryInterface) *Service {
	return &Service{
		Repository: repository,
	}
}

func (s *Service) SignIn(req *model.UserSignInReq) (res model.UserSignInRes, err error) {
	user, err := s.Repository.FirstByEmail(&req.Body.UserName)
	if err != nil {
		err = errors.New("User not found")
		return
	}

	md5password := md5.Sum([]byte(req.Body.Password))
	pass := hex.EncodeToString(md5password[:])

	if user.Password != pass {
		err = errors.New("Password didn't match")
		return
	}

	claims := model.Claims{
		ID:       user.ID,
		UserName: user.UserName,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * time.Duration(1)).Unix(),
		},
	}

	ss, err := encrypt.NewWithClaims(&claims)
	if err != nil {
		return
	}
	if err = s.Repository.UpdateToken(&user, &ss); err != nil {
		return
	}
	res.Body.Token = ss
	return res, nil
}

func (s *Service) Auth(bearerToken string) (admin model.User, err error) {
	tokenRaw, claims, err := encrypt.Parse(bearerToken)
	if err != nil {
		return
	}
	id := uint64(claims["id"].(float64))
	admin, err = s.Repository.FirstByID(&id)
	if err != nil {
		if err.Error() == "record not found" {
			err = errors.New("User not found")
		}
		return
	}
	if admin.Token != tokenRaw {
		err = errors.New("User has signout")
		return
	}
	return
}

func (s *Service) SignOut(admin *model.User) error {
	if err := s.Repository.DeleteToken(admin); err != nil {
		return err
	}
	return nil
}
