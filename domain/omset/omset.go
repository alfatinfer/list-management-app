package omset

import (
	"majoo/domain/omset/model"
	"majoo/domain/omset/repository"
)

type Service struct {
	Repository repository.RepositoryInterface
}

func NewOmsetService(repository repository.RepositoryInterface) *Service {
	return &Service{
		Repository: repository,
	}
}

func (s *Service) GetList(offset, limit, id int, req model.ListReq) ([]model.OmsetOutlet, int64, error) {
	data, totalRow, err := s.Repository.GetList(offset, limit, id, req.Start, req.End)
	if err != nil {
		return data, totalRow, err
	}
	if data == nil {
		data1, totalRow, err := s.Repository.GetName(offset, limit, id)
		if err != nil {
			return data, totalRow, err
		}
		data = data1
		return data, totalRow, nil
	}
	return data, totalRow, nil
}

func (s *Service) GetMerchantList(offset, limit, id int, req model.ListReq) ([]model.OmsetMerchant, int64, error) {
	data, totalRow, err := s.Repository.GetMerchantList(offset, limit, id, req.Start, req.End)
	if err != nil {
		return data, totalRow, err
	}
	if data == nil {
		data1, totalRow, err := s.Repository.GetNameMerchant(offset, limit, id)
		if err != nil {
			return data, totalRow, err
		}
		data = data1
		return data, totalRow, nil
	}
	return data, totalRow, nil
}
