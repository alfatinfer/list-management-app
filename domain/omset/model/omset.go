package model

type OmsetOutlet struct {
	MerchantName string
	OutletName   string
	Omset        float64
}

func (OmsetOutlet) TableName() string {
	return "transactions"
}

type OmsetMerchant struct {
	MerchantName string
	Omset        float64
}

func (OmsetMerchant) TableName() string {
	return "transactions"
}

type ListReq struct {
	Start string `form:"start"`
	End   string `form:"end"`
}
