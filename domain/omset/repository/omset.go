package repository

import (
	"majoo/domain/omset/model"

	"gorm.io/gorm"
)

type (
	RepositoryInterface interface {
		GetList(offset, limit, merchantId int, startDate, endDate string) ([]model.OmsetOutlet, int64, error)
		GetMerchantList(offset, limit, merchantId int, startDate, endDate string) ([]model.OmsetMerchant, int64, error)
		GetName(offset, limit, merchantId int) ([]model.OmsetOutlet, int64, error)
		GetNameMerchant(offset, limit, merchantId int) ([]model.OmsetMerchant, int64, error)
	}

	repository struct {
		DB *gorm.DB
	}
)

func NewOmsetRepository(DB *gorm.DB) RepositoryInterface {
	return &repository{
		DB: DB,
	}
}

func (r *repository) GetList(offset, limit, merchantId int, startDate, endDate string) ([]model.OmsetOutlet, int64, error) {
	var (
		omset    []model.OmsetOutlet
		totalRow int64
	)
	queryToDatabse := r.DB.Debug().Select("transactions.merchant_id, outlet_id, merchants.merchant_name,SUM (bill_total) as omset, outlets.outlet_name").
		Model(&omset).
		Joins("RIGHT JOIN outlets on outlets.id = transactions.outlet_id").
		Joins("RIGHT JOIN merchants on merchants.id = transactions.merchant_id").
		Where("merchants.id = ? AND transactions.updated_at >= ? AND transactions.updated_at <= ?", merchantId, startDate, endDate).
		Group("transactions.merchant_id, outlet_id, merchants.merchant_name, outlets.outlet_name")
	queryToDatabse.Count(&totalRow)

	return omset, totalRow, queryToDatabse.Limit(limit).Offset(offset).Scan(&omset).Error
}

func (r *repository) GetName(offset, limit, merchantId int) ([]model.OmsetOutlet, int64, error) {
	var (
		omset    []model.OmsetOutlet
		totalRow int64
	)
	queryToDatabse := r.DB.Debug().Select("transactions.merchant_id, outlet_id, merchants.merchant_name, outlets.outlet_name").
		Model(&omset).
		Joins("INNER JOIN outlets on outlets.id = transactions.outlet_id").
		Joins("INNER JOIN merchants on merchants.id = transactions.merchant_id").
		Where("merchants.id = ?", merchantId).
		Group("transactions.merchant_id, outlet_id, merchants.merchant_name, outlets.outlet_name")
	queryToDatabse.Count(&totalRow)

	return omset, totalRow, queryToDatabse.Limit(limit).Offset(offset).Scan(&omset).Error
}

func (r *repository) GetMerchantList(offset, limit, merchantId int, startDate, endDate string) ([]model.OmsetMerchant, int64, error) {
	var (
		omset    []model.OmsetMerchant
		totalRow int64
	)
	queryToDatabse := r.DB.Debug().Select("transactions.merchant_id, merchants.merchant_name, SUM (bill_total) as omset").
		Model(&omset).
		Joins("RIGHT JOIN merchants on merchants.id = transactions.merchant_id").
		Where("merchants.id = ? AND transactions.updated_at >= ? AND transactions.updated_at <= ?", merchantId, startDate, endDate).
		Group("transactions.merchant_id, merchants.merchant_name")
	queryToDatabse.Count(&totalRow)

	return omset, totalRow, queryToDatabse.Limit(limit).Offset(offset).Find(&omset).Error
}

func (r *repository) GetNameMerchant(offset, limit, merchantId int) ([]model.OmsetMerchant, int64, error) {
	var (
		omset    []model.OmsetMerchant
		totalRow int64
	)
	queryToDatabse := r.DB.Debug().Select("transactions.merchant_id, merchants.merchant_name").
		Model(&omset).
		Joins("INNER JOIN merchants on merchants.id = transactions.merchant_id").
		Where("merchants.id = ?", merchantId).
		Group("transactions.merchant_id, merchants.merchant_name")
	queryToDatabse.Count(&totalRow)

	return omset, totalRow, queryToDatabse.Limit(limit).Offset(offset).Find(&omset).Error
}
