package config

import (
	"log"
	"majoo/app/controller/healthcheck"
	"majoo/app/controller/root"
	"majoo/app/controller/user"
	"majoo/app/controller/omset"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func Router(db *gorm.DB) {
	rootController := root.NewController()
	healthcheckcController := healthcheck.NewController()
	router := gin.Default()
	router.Use(corsConfig)
	router.GET("/", rootController.Index)
	router.GET("health-check", healthcheckcController.Index)

	v1 := router.Group("v1")
	{
		usersGroup := v1.Group("users")
		{
			userController := user.NewController(db)
			usersGroup.POST("sign-in", userController.SignIn)
			usersGroup.DELETE("sign-out", userController.SignOut)
		}
		omsetGroup := v1.Group("omset")
		{
			omsetController := omset.NewController(db)
			omsetGroup.GET("", omsetController.List)
		}
	}
	if err := router.Run(); err != nil {
		log.Fatal(err)
	}
}
