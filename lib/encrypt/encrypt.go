package encrypt

import (
	"strings"

	"github.com/dgrijalva/jwt-go"
)


func NewWithClaims(claims jwt.Claims) (ss string, err error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err = token.SignedString([]byte("majoo"))
	return
}

func Parse(auth string) (tokenRaw string, claims jwt.MapClaims, err error) {
	ss := strings.ReplaceAll(auth, "Bearer ", "")
	token, err := jwt.Parse(ss, func(token *jwt.Token) (interface{}, error) {
		return []byte("majoo"), nil
	})
	if err != nil {
		return
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return token.Raw, claims, nil
	}
	return
}
