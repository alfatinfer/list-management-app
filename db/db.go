package db

import (
	"database/sql"
	"log"

	_ "github.com/joho/godotenv/autoload"
	_ "github.com/lib/pq"
)

var SqlDb *sql.DB

func init() {
	var err error
	if SqlDb, err = sql.Open("postgres","postgresql://postgres:Postgres!23@localhost:5432/majoo?sslmode=disable"); err != nil {
		log.Fatal(err)
	}
}
