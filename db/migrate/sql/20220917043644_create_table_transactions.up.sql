CREATE TABLE transactions (
    id bigint NOT NULL,
    merchant_id bigint NOT NULL,
    outlet_id bigint NOT NULL,
    bill_total decimal NOT NULL,
    created_at timestamp NOT NULL,
    created_by bigint NOT NULL,
    updated_at timestamp NOT NULL,
    updated_by bigint NOT NULL,
    PRIMARY KEY (id)
  );