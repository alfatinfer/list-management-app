CREATE TABLE users (
  id bigint NOT NULL,
  name varchar(45) DEFAULT NULL,
  user_name varchar(45) DEFAULT NULL,
  password varchar DEFAULT NULL,
  created_at timestamp NOT NULL DEFAULT now(),
  created_by bigint NOT NULL,
  updated_at timestamp NOT NULL DEFAULT now(),
  updated_by bigint NOT NULL,
  PRIMARY KEY (id)
);