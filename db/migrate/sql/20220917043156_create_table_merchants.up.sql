CREATE TABLE merchants (
    id serial NOT NULL,
    user_id bigint NOT NULL,
    merchant_name varchar(40) NOT NULL,
    created_at timestamp NOT NULL DEFAULT now(),
    created_by bigint NOT NULL,
    updated_at timestamp NOT NULL DEFAULT now(),
    updated_by bigint NOT NULL,
    PRIMARY KEY (id)
  );