run_local:
	go run main.go

run_db_migrate_up:
	go run db/migrate/up/up.go

run_db_migrate_down:
	go run db/migrate/down/down.go

run_seed_majoo:
	psql -U postgres majoo < db/seed.sql